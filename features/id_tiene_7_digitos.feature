Feature:  ID tiene 7 digitos?
  Para el ID es el numero de cedula que tiene 7 digitos

  Scenario: ID tiene 0 digitos
    Given ID tiene 0 digitos
    When Se pregunta si ID tiene 7 digitos
    Then Deberia salir "Invalido ID"
    
 Scenario: ID tiene menos de 7 digitos
    Given ID tiene 6 digitos
    When Se pregunta si ID tiene 7 digitos
    Then Deberia salir "Invalido ID"

 Scenario: ID tiene  7 digitos
    Given ID tiene 7 digitos
    When Se pregunta si ID tiene 7 digitos
    Then Deberia salir "Valido"