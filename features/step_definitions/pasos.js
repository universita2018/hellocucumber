const assert = require('assert');
const { Given, When, Then } = require('cucumber');


 function digito(digitos){
  if(digitos==0){
      return "invalido ID vacio";
  }
  else{
    if(digitos==7){
      return "valido ID";
    }
    else{
      return "invalido ID";
    }
  }
}
Given ('ID tiene {int} digitos', function(digitos){
  this.digitos= digitos;
});

When('Se pregunta  si ID tiene {int}  digitos',function(int){
  return 'pending';
});
Then('Deberia salir {string}',function(expectedAnswer){
  assert.equal(this.actualAnswer,expectedAnswer);
});

